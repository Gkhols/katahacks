class KataController < ApplicationController

  def index
    response = HTTParty.get('https://djasayu-hack.herokuapp.com/phone-info/?name='+params[:hp].to_s)
    @resp = response.body.as_json
    render json: @resp
  end

  def kata
    token = "587f2c96-76a6-4416-8568-63f49fdaaf2b"
    response = HTTParty.post(
      "https://geist.kata.ai/nlus/djasayu:djasayuApp/predict",
      headers: {
        Authorization: "Bearer #{token}"
      },
      body: {
        text: params[:text].to_s
      }
    )
    render json: response.body.as_json
  end

  def app
    token = "587f2c96-76a6-4416-8568-63f49fdaaf2b"
    response = HTTParty.post(
      "https://geist.kata.ai/nlus/djasayu:djasayuApp/predict",
      headers: {
        Authorization: "Bearer #{token}"
      },
      body: {
        text: params[:text].to_s
      }
    ).to_s

    json = JSON.parse(response)
    #harga = json['result']['harga'][0]['resolved']['dictKey'].to_s
    #brand = json['result']['brand'][0]['resolved']['dictKey'].to_s

    intent = json['result']['intent'][0]['value']
    val_intent = intent.to_s
    print val_intent

    if val_intent == "getListHargaFromMerksearch"
      harga = json['result']['harga'][0]['resolved']['dictKey'].to_s
      brand = json['result']['brand'][0]['resolved']['dictKey'].to_s
      response = HTTParty.get('http://djasayu-hack.herokuapp.com/get-phone-by-price/?brand='+brand+'&price_limit='+harga).to_s
      responsetype = 'listharga'
      resp = JSON.parse(response)
      #@resp = response.body.as_json
      render json: {result: resp, type:responsetype}
    elsif val_intent == "getInfoHp"
      device = json['result']['device_name'][0]['value'].to_s
      response = HTTParty.get('https://djasayu-hack.herokuapp.com/phone-info/?name='+device).to_s
      responsetype = 'infoharga'
      resp = JSON.parse(response)
      #@resp = response.body.as_json
      render json: {result: resp,type:responsetype}
    elsif val_intent == "compareHp"
      device1 = json['result']['device_name'][0]['value'].to_s
      device2 = json['result']['device_name'][1]['value'].to_s
      response = HTTParty.get('https://djasayu-hack.herokuapp.com/compare-phone/?device1='+device1+'&device2='+device2).to_s
      responsetype = 'bandinghp'
      resp = JSON.parse(response)
      #@resp = response.body.as_json
      render json: {result: resp,type:responsetype}
    end

    end
end
